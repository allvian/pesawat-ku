<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon">
      &nbsp&nbsp&nbsp<p class="brand fas fa-paper-plane"></p>&nbsp&nbsp&nbsp
      </span>
    </button>
    <a class="navbar-brand" href="#">PesawatKu</a>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active"  href="index.php?include=home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="index.php?include=pesawat1">Data Pesawat</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="index.php?include=bagasi1">Bagasi</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="index.php?include=pemesanan1">Pemesanan</a>
        </li>
      </ul>
      <form class="ml-auto d-flex">
        <a href="admin/index.php" class="btn btn-success">
					<i class="fas fa-plus"></i> LOGIN
				</a>
      </form>
    </div>
  </div>
</nav>