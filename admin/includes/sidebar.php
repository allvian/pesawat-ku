<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
		&nbsp&nbsp&nbsp<p class="brand fas fa-paper-plane"></p>&nbsp&nbsp&nbsp
      <span class="brand-text font-weight-light"><b>PesawatKu<b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<li class="nav-item">
				<a href="index.php?include=profil" class="nav-link">
					<i class="nav-icon fas fa-user"></i>
					<p>Profil</p>
				</a>
			</li>
			<li class="nav-item">
				<a href="index.php?include=pesawat" class="nav-link">
					<i class="nav-icon fas fa-plane"></i>
					<p>Data Pesawat</p>
	            </a>
			</li>
			<li class="nav-item">
				<a href="index.php?include=pemesanan" class="nav-link">
					<i class="nav-icon fas fa-clipboard-list"></i>
					<p>Pemesanan</p>
	            </a>
			</li>
			<li class="nav-item">
				<a href="index.php?include=bagasi" class="nav-link">
					<i class="nav-icon fas fa-luggage-cart  "></i>
					<p>Bagasi</p>
	            </a>
			</li>
			<?php 
				if (isset($_SESSION['level'])){
					if ($_SESSION['level']=="superadmin"){?>
						<li class="nav-item">
							<a href="index.php?include=user" class="nav-link">
								<i class="nav-icon fas fa-user-cog"></i>
								<p>Pengaturan User</p>
							</a>
						</li>
					<?php }?>
				<?php }?>
			<li class="nav-item">
				<a href="index.php?include=sign_out" class="nav-link">
					<i class="nav-icon fas fa-sign-out-alt"></i>
					<p>Sign Out</p>
	            </a>
			</li>	
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>