<!DOCTYPE html>
<?php 
	session_start();
	include('../koneksi/koneksi.php');
	if(isset($_GET["include"])){
		$include = $_GET["include"];
		if($include=="konfirmasi_login"){
			include("include/konfirmasi_login.php");
		}else if($include=="sign_out"){
			include("include/sign_out.php");
		}else if($include=="konfirmasi_tambah_pesawat"){
			include("include/konfirmasi_tambah_pesawat.php");
		}else if($include=="konfirmasi_edit_pesawat"){
			include("include/konfirmasi_edit_pesawat.php");
		}else if($include=="konfirmasi_tambah_pemesanan"){
			include("include/konfirmasi_tambah_pemesanan.php");
		}else if($include=="konfirmasi_tambah_bagasi"){
			include("include/konfirmasi_tambah_bagasi.php");
		}else if($include=="konfirmasi_edit_profil"){
			include("include/konfirmasi_edit_profil.php");
		}else if($include=="konfirmasi_tambah_user"){
			include("include/konfirmasi_tambah_user.php");
		}else if($include=="konfirmasi_edit_pemesanan"){
			include("include/konfirmasi_edit_pemesanan.php");
		}
	}
?>
<html>
	<head>
		<?php include("includes/head.php") ?>
	</head>
	<?php
		//jika ada get include
		if(isset($_GET["include"])){
		  $include = $_GET["include"];
		  //jika ada session id admin
			if(isset($_SESSION['id_admin'])){
			//pemanggilan ke halaman-halaman menu admin
	?>
				<body class="hold-transition sidebar-mini layout-fixed">
					<div class="wrapper">
						<?php include("includes/header.php") ?>
						<?php include("includes/sidebar.php") ?>
						<!-- Content Wrapper. Contains page content -->
							<div class="content-wrapper">
								<?php
								   if($include=="pesawat"){
										include("include/pesawat.php");
								   }else if($include=="tambah_pesawat"){
										include("include/tambah_pesawat.php");
								   }else if($include=="edit_pesawat"){
										include("include/edit_pesawat.php");
								   }else if($include=="pemesanan"){
										include("include/pemesanan.php");
							   	   }else if($include=="tambah_pemesanan"){
										include("include/tambah_pemesanan.php");
								   }else if($include=="bagasi"){
										include("include/bagasi.php");
							   	   }else if($include=="tambah_bagasi"){
										include("include/tambah_bagasi.php");
								   }else if($include=="user"){
										include("include/user.php");
								   }else if($include=="tambah_user"){
										include("include/tambah_user.php");
								   }else if($include=="edit_user"){
										include("include/edit_user.php");
								   }else if($include=="edit_profil"){
										include("include/edit_profil.php");
								   }else if($include=="edit_pemesanan"){
										include("include/edit_pemesanan.php");
								   }else{
										include("include/profil.php");
									}
								?>
								<!---/Content--->
							</div>
							<!---/Content wrapper--->
							<?php include("includes/footer.php") ?>
					</div>
						<!-- ./wrapper -->
						<?php include("includes/script.php") ?>
				</body>
		<?php    
			}
		}else{
			//jika tidak ada include pemanggilan halaman form login
			include("include/login.php");
		}
		?>
</html>