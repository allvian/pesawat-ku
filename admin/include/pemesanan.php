<?php
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$kode_tiket = $_GET['data'];
			//hapus jurusan
			$sql_dh = "delete from `tbl_pemesanan` 
			where `kode_tiket` = '$kode_tiket'";
			mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<section class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3>
					<i class="fas fa-database"></i>Pemesanan Tiket Pesawat
				</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item">
						<a href="#">Home</a>
					</li>
					<li class="breadcrumb-item active">Pemesanan Tiket Pesawat</li>
				</ol>
			</div>
        </div>
    </div><!-- /.container-fluid -->
</section>
    <!-- Main content -->
<section class="content">
    <div class="card">
        <div class="card-header">
			<h3 class="card-title" style="margin-top:5px;">
				<i class="fas fa-list-ul"></i> Daftar Pemesanan
			</h3>
            <div class="card-tools">
                <a href="index.php?include=tambah_pemesanan" class="btn btn-sm btn-info float-right">
					<i class="fas fa-plus"></i> Tambah Pemesanan
				</a>
            </div>
        </div>
            <!-- /.card-header -->
        <div class="card-body">
			<div class="col-sm-12">
				<?php if(!empty($_GET['notif'])){?>
					<?php if($_GET['notif']=="tambahberhasil"){?>
						<div class="alert alert-success" role="alert">Data Berhasil Ditambahkan</div>
					<?php } else if($_GET['notif']=="editberhasil"){?>
						<div class="alert alert-success" role="alert">Data Berhasil Diubah</div>
					<?php }?>
				<?php }?>
			</div>
			<div class="col-md-12">
				<form method="post" action="index.php?include=pemesanan">
					<div class="row">
						<div class="col-md-4 bottom-10">
							<input type="text" class="form-control" id="kata_kunci" name="katakunci">
						</div>
						<div class="col-md-5 bottom-10">
							<button type="submit" class="btn btn-primary">
								<i class="fas fa-search"></i>  
								Search
							</button>
						</div>
					</div><!-- .row -->
				</form>
			</div>
			<br> 
            <table class="table table-bordered">
                <thead>                  
					<tr>
						<th width="15%">Kode Tiket</th>
						<th width="30%">Nama Pesawat</th>
                        <th width="25%">Tipe Pesawat</th>
                        <th width="20%">Jam Penerbangan</th>
						<th width="10%">
							<center>Aksi</center>
						</th>
                    </tr>
                </thead>
                <tbody>
					<?php
						$batas = 2;
							if(!isset($_GET['halaman'])){
								 $posisi = 0;
								 $halaman = 1;	
							}else{
								 $halaman = $_GET['halaman'];
								 $posisi = ($halaman-1) * $batas;
							}
						//menampilkan data hobi
						$sql_h = "SELECT * FROM `tb_pemesanan`";
						if (isset($_POST["katakunci"])){
							  $katakunci_pemesanan = $_POST["katakunci"];
							  $_SESSION['katakunci_pemesanan'] = $katakunci_pemesanan;
							  $sql_h .= " where `kode_tiket` LIKE '%$katakunci_pemesanan%'";
						} 
						$sql_h .= " order by `kode_tiket` limit $posisi, $batas ";
						$query_h = mysqli_query($koneksi,$sql_h);
						$no=$posisi+1;
						while($data_h = mysqli_fetch_row($query_h)){
							$kode_tiket = $data_h[0];
                            $nama_pesawat = $data_h[1];
                            $tipe_pesawat = $data_h[2];
                            $jam_penerbangan = $data_h[3];
					?>		
					<tr>
						<td>
							<?php echo $kode_tiket;?>
						</td>
						<td>
							<?php echo $nama_pesawat;?>
						</td>
                        <td>
							<?php echo $tipe_pesawat;?>
						</td>
                        <td>
							<?php echo $jam_penerbangan;?>
						</td>
						<td>
							<a href="index.php?include=edit_pemesanan&data=<?php echo $kode_tiket;?>" class="btn btn-xs btn-info" title="Edit">
								<i class="fas fa-edit"></i>
								Edit
							</a>
							<a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_pesawat; ?>?')) window.location.href = 'index.php?include=pesawat&aksi=hapus&data=<?php echo $kode_tiket;?>'" class="btn btn-xs btn-warning">
								<i class="fas fa-trash"></i>
								Hapus 
							</a> 
						</td>
					</tr>
					<?php
						$no++;
						}?>
						
					<?php
						//hitung jumlah semua data 
						$sql_jum = "SELECT * FROM `tb_pemesanan`"; 
							if (isset($_SESSION["katakunci"])){
								  $katakunci_pemesanan = $_SESSION["katakunci"];
								  $sql_jum .= " where `kode_tiket` LIKE '%$katakunci_pemesanan%'";
							} 
						$sql_jum .= " order by `kode_tiket`";
						$query_jum = mysqli_query($koneksi,$sql_jum);
						$jum_data = mysqli_num_rows($query_jum);
						$jum_halaman = ceil($jum_data/$batas);
					?>	
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
				<?php 
					if($jum_halaman==0){
						//tidak ada halaman
					}else if($jum_halaman==1){
						echo "<li class='page-item'><a class='page-link'>1</a></li>";
					}else{
						$sebelum = $halaman-1;
						$setelah = $halaman+1;                  
						if($halaman!=1){
							echo "<li class='page-item'><a class='page-link' href='index.php?include=pemesanan&halaman=1'>First</a></li>";
							echo "<li class='page-item'><a class='page-link' href='index.php?include=pemesanan&halaman=$sebelum'>«</a></li>";
						}
							//menampilkan angka halaman
						for($i=1; $i<=$jum_halaman; $i++){
							if($i!=$halaman){
								echo "<li class='page-item'><a class='page-link' href='index.php?include=pemesanan&halaman=$i'>$i</a></li>";
							}else{
								echo "<li class='page-item'><a class='page-link'>$i</a></li>";
							}
						}
						if($halaman!=$jum_halaman){
							echo "<li class='page-item'><a class='page-link'  href='index.php?include=pemesanan&halaman=$setelah'>»</a></li>";
							echo "<li class='page-item'><a class='page-link' href='index.php?include=pemesanan&halaman=$jum_halaman'>Last</a></li>";
						}
					}
				?>
			</ul>
        </div>
    </div>
    <!-- /.card -->
</section>

