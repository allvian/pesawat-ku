    <!-- Main content -->
<section class="content">
    <div class="card">
        <div class="card-header shadow-sm p-3 mb-5 bg-white rounded">
			<div class="container ">
				<h3 class="card-title" style="margin-top:5px; margin-left:425px;">
					<i class="fas fa-list-ul"></i> Daftar Bagasi
				</h3>
			</div>
            <div class="container card-tools "style="margin-left:78%;">
                <a href="index.php?include=tambah_bagasi1" class="btn btn-sm btn-info">
					<i class="fas fa-plus"></i> Tambah Bagasi
				</a>
            </div>
        </div>
            <!-- /.card-header -->
        <div class="card-body container">
			<div class="col-sm-12">
				<?php if(!empty($_GET['notif'])){?>
					<?php if($_GET['notif']=="tambahberhasil"){?>
						<div class="alert alert-success" role="alert">Data Berhasil Ditambahkan</div>
					<?php } else if($_GET['notif']=="editberhasil"){?>
						<div class="alert alert-success" role="alert">Data Berhasil Diubah</div>
					<?php }?>
				<?php }?>
			</div>
			<div class="col-md-12">
				<form method="post" action="index.php?include=bagasi1">
					<div class="row">
						<div class="col-md-4 bottom-10">
							<input type="text" class="form-control" id="kata_kunci" name="katakunci">
						</div>
						<div class="col-md-5 bottom-10">
							<button type="submit" class="btn btn-primary">
								<i class="fas fa-search"></i>  
								Search
							</button>
						</div>
					</div><!-- .row -->
				</form>
			</div>
			<br> 
            <table class="table table-bordered shadow p-3 mb-5 bg-white rounded">
                <thead>                  
					<tr>
						<th width="25%">Kode Bagasi</th>
						<th width="35%">Kode Pesawat</th>
                        <th width="25%">Berat Bagasi</th>
                    </tr>
                </thead>
                <tbody >
					<?php
						$batas = 2;
							if(!isset($_GET['halaman'])){
								 $posisi = 0;
								 $halaman = 1;	
							}else{
								 $halaman = $_GET['halaman'];
								 $posisi = ($halaman-1) * $batas;
							}
						//menampilkan data hobi
						$sql_h = "SELECT `kode_bagasi`, `kode_pesawat`, `berat_bagasi` FROM `tb_bagasi`";
						if (isset($_POST["katakunci"])){
							  $katakunci_bagasi = $_POST["katakunci"];
							  $_SESSION['katakunci_bagasi'] = $katakunci_bagasi;
							  $sql_h .= " where `kode_bagasi` LIKE '%$katakunci_bagasi%'";
						} 
						$sql_h .= " order by `kode_bagasi` limit $posisi, $batas ";
						$query_h = mysqli_query($koneksi,$sql_h);
						$no=$posisi+1;
						while($data_h = mysqli_fetch_row($query_h)){
							$kode_bagasi = $data_h[0];
                            $kode_pesawat = $data_h[1];
                            $berat_bagasi = $data_h[2];
					?>
							
					<tr>
						<td>
							<?php echo $kode_bagasi;?>
						</td>
						<td>
							<?php echo $kode_pesawat;?>
						</td>
                        <td>
							<?php echo $berat_bagasi;?>
						</td>
					</tr>
					<?php
						$no++;
						}?>
					<?php
						//hitung jumlah semua data 
						$sql_jum = "SELECT `kode_bagasi`, `kode_pesawat`, `berat_bagasi` FROM `tb_bagasi`"; 
							if (isset($_SESSION["katakunci"])){
								  $katakunci_bagasi = $_SESSION["katakunci"];
								  $sql_jum .= " where `tb_bagasi` LIKE '%$katakunci_bagasi%'";
							} 
						$sql_jum .= " order by `kode_bagasi`";
						$query_jum = mysqli_query($koneksi,$sql_jum);
						$jum_data = mysqli_num_rows($query_jum);
						$jum_halaman = ceil($jum_data/$batas);
					?>	
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm"style="margin-left:475px">
				<?php 
					if($jum_halaman==0){
						//tidak ada halaman
					}else if($jum_halaman==1){
						echo "<li class='page-item'><a class='page-link'>1</a></li>";
					}else{
						$sebelum = $halaman-1;
						$setelah = $halaman+1;                  
						if($halaman!=1){
							echo "<li class='page-item'><a class='page-link' href='index.php?include=bagasi1&halaman=1'>First</a></li>";
							echo "<li class='page-item'><a class='page-link' href='index.php?include=bagasi1&halaman=$sebelum'>«</a></li>";
						}
							//menampilkan angka halaman
						for($i=1; $i<=$jum_halaman; $i++){
							if($i!=$halaman){
								echo "<li class='page-item'><a class='page-link' href='index.php?include=bagasi1&halaman=$i'>$i</a></li>";
							}else{
								echo "<li class='page-item'><a class='page-link'>$i</a></li>";
							}
						}
						if($halaman!=$jum_halaman){
							echo "<li class='page-item'><a class='page-link'  href='index.php?include=bagasi1&halaman=$setelah'>»</a></li>";
							echo "<li class='page-item'><a class='page-link' href='index.php?include=bagasi1&halaman=$jum_halaman'>Last</a></li>";
						}
					}
				?>
			</ul>
        </div>
    </div>
    <!-- /.card -->
</section>

