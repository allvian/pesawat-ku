<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
			<div class="col-sm-6">
				<h3>
					<i class="fas fa-plus"></i> Tambah Pemesanan
				</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item">
						<a href="#">Home</a>
					</li>
					<li class="breadcrumb-item">
						<a href="index.php?include=pemesanan">Data Pemesanan</a>
					</li>
					<li class="breadcrumb-item active">Tambah Pemesanan</li>
				</ol>
			</div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"style="margin-top:5px;">
				<i class="far fa-list-alt"></i> Form Tambah Pemesanan
			</h3>
			<div class="card-tools">
				<a href="index.php?include=pemesanan" class="btn btn-sm btn-warning float-right">
					<i class="fas fa-arrow-alt-circle-left"></i> Kembali
				</a>
			</div>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		</br></br>
		<div class="col-sm-10">
			<?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
				<?php if($_GET['notif']=="tambahkosong"){?>
					<div class="alert alert-danger" role="alert">Maaf data 
						<?php echo $_GET['jenis'];?> wajib di isi
					</div>
				<?php }?>
			<?php }?>
		</div>
		<form class="form-horizontal" method="post" enctype="multipart/form-data" action="index.php?include=konfirmasi_tambah_pemesanan">
			<div class="card-body">
			   <div class="form-group row">
					<label for="judul" class="col-sm-12 col-form-label">
						<span class="text-info">
							<i class="fas fa-user-circle"></i> 
							<u> Data Pemesanan</u>
						</span>
					</label>
			   </div>
			   <div class="form-group row">
					<label for="tiket" class="col-sm-3 col-form-label">Kode Tiket</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" name="tiket" id="tiket" value="<?php if(!empty($_SESSION['tiket'])){ echo $_SESSION['tiket'];} ?>">
					</div>
				</div>
				<div class="form-group row">
					<label for="nama" class="col-sm-3 col-form-label">Nama Pesawat</label>
					<div class="col-sm-7">
						<select class="form-control" id="nama" name="nama">
							<option value="0">- Pilih Pesawat -</option>
							   <?php
								   $sql_j = "select `kode_pesawat`, `nama_pesawat` from `tb_pesawat` order by `kode_pesawat`";
								   $query_j = mysqli_query($koneksi,$sql_j);
										while($data_j = mysqli_fetch_row($query_j)){
											$kode_pesawat = $data_j[0];
											$nama_pesawat = $data_j[1];
							   ?>
							<option value="<?php echo $kode_pesawat;?>"<?php if(!empty($_SESSION['nama'])){if($kode_pesawat==$_SESSION['nama']){?> selected="selected" <?php }}?>>
								<?php echo $nama_pesawat;?>
										<?php }?>
							</option>
						</select>
					</div>
				</div>
                <div class="form-group row">
					<label for="tipe" class="col-sm-3 col-form-label">Tipe Pesawat</label>
					<div class="col-sm-7">
						<select class="form-control" id="tipe" name="tipe">
							<option value="0">- Pilih Tipe Pesawat -</option>
							   <?php
								   $sql_j = "select `kode_tipe_pesawat`, `tipe_pesawat` from `tb_tipe_pesawat` order by `kode_tipe_pesawat`";
								   $query_j = mysqli_query($koneksi,$sql_j);
										while($data_j = mysqli_fetch_row($query_j)){
											$kode_tipe_pesawat = $data_j[0];
											$tipe_pesawat = $data_j[1];
							   ?>
							<option value="<?php echo $kode_tipe_pesawat;?>"<?php if(!empty($_SESSION['tipe'])){if($kode_tipe_pesawat==$_SESSION['tipe']){?> selected="selected" <?php }}?>>
								<?php echo $tipe_pesawat;?>
										<?php }?>
							</option>
						</select>
					</div>
				</div>
                <div class="form-group row">
					<label for="jam" class="col-sm-3 col-form-label">Jam Penerbangan</label>
					<div class="col-sm-7">
						<select class="form-control" id="jam" name="jam">
							<option value="0">- Pilih Jam Penerbangan -</option>
							   <?php
								   $sql_j = "select `kode_penerbangan`, `jam_penerbangan` from `tb_penerbangan` order by `kode_penerbangan`";
								   $query_j = mysqli_query($koneksi,$sql_j);
										while($data_j = mysqli_fetch_row($query_j)){
											$kode_penerbangan = $data_j[0];
											$jam_penerbangan = $data_j[1];
							   ?>
							<option value="<?php echo $kode_penerbangan;?>"<?php if(!empty($_SESSION['jam'])){if($kode_penerbangan==$_SESSION['jam']){?> selected="selected" <?php }}?>>
								<?php echo $jam_penerbangan;?>
										<?php }?>
							</option>
						</select>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-info float-right">
							<i class="fas fa-plus"></i> 
							Tambah
						</button>
					</div>  
				</div>
			<!-- /.card-footer -->
			 </div>
		</form>
	</div>
    <!-- /.card -->
</section>