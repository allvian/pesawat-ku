
<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header shadow-sm p-3 mb-5 bg-white rounded">
			<div class="container ">
				<h3 class="card-title"style="margin-top:5px; margin-left:385px">
					<i class="far fa-list-alt"></i> Form Tambah Pemesanan
				</h3>
			</div>
		</div>
		<div class="card-tools " style="margin-left:78%;">
			<a href="index.php?include=pemesanan1" class="btn btn-sm btn-warning">
				<i class="fas fa-arrow-alt-circle-left"></i> Kembali
			</a>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body container">
			<div class="col-sm-10">
				<?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
					<?php if($_GET['notif']=="tambahkosong"){?>
						<div class="alert alert-danger" role="alert">Maaf data 
							<?php echo $_GET['jenis'];?> wajib di isi
						</div>
					<?php }?>
				<?php }?>
			</div>	
			<form class="form-horizontal" method="post" enctype="multipart/form-data" action="index.php?include=konfirmasi_tambah_pemesanan1">
				<div class="card-body container shadow p-3 mb-5 bg-white rounded">
					<div class="form-group row">
						<label for="tiket" class="col-sm-3 col-form-label">Kode Tiket</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="tiket" id="tiket" value="<?php if(!empty($_SESSION['tiket'])){ echo $_SESSION['tiket'];} ?>">
							</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Nama Pesawat</label>
						<div class="col-sm-7">
							<select class="form-control" id="nama" name="nama">
								<option value="0">- Pilih Pesawat -</option>
									<?php
										$sql_j = "select `kode_pesawat`, `nama_pesawat` from `tb_pesawat` order by `kode_pesawat`";
										$query_j = mysqli_query($koneksi,$sql_j);
												while($data_j = mysqli_fetch_row($query_j)){
													$kode_pesawat = $data_j[0];
													$nama_pesawat = $data_j[1];
									?>
									<option value="<?php echo $kode_pesawat;?>"<?php if(!empty($_SESSION['nama'])){if($kode_pesawat==$_SESSION['nama']){?> selected="selected" <?php }}?>>
										<?php echo $nama_pesawat;?>
												<?php }?>
									</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="tipe" class="col-sm-3 col-form-label">Tipe Pesawat</label>
						<div class="col-sm-7">
							<select class="form-control" id="tipe" name="tipe">
								<option value="0">- Pilih Tipe Pesawat -</option>
									<?php
										$sql_j = "select `kode_tipe_pesawat`, `tipe_pesawat` from `tb_tipe_pesawat` order by `kode_tipe_pesawat`";
										$query_j = mysqli_query($koneksi,$sql_j);
												while($data_j = mysqli_fetch_row($query_j)){
													$kode_tipe_pesawat = $data_j[0];
													$tipe_pesawat = $data_j[1];
									?>
									<option value="<?php echo $kode_tipe_pesawat;?>"<?php if(!empty($_SESSION['tipe'])){if($kode_tipe_pesawat==$_SESSION['tipe']){?> selected="selected" <?php }}?>>
										<?php echo $tipe_pesawat;?>
												<?php }?>
									</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="jam" class="col-sm-3 col-form-label">Jam Penerbangan</label>
						<div class="col-sm-7">
							<select class="form-control" id="jam" name="jam">
								<option value="0">- Pilih Jam Penerbangan -</option>
								<?php
									$sql_j = "select `kode_penerbangan`, `jam_penerbangan` from `tb_penerbangan` order by `kode_penerbangan`";
									$query_j = mysqli_query($koneksi,$sql_j);
											while($data_j = mysqli_fetch_row($query_j)){
												$kode_penerbangan = $data_j[0];
												$jam_penerbangan = $data_j[1];
								?>
								<option value="<?php echo $kode_penerbangan;?>"<?php if(!empty($_SESSION['jam'])){if($kode_penerbangan==$_SESSION['jam']){?> selected="selected" <?php }}?>>
									<?php echo $jam_penerbangan;?>
											<?php }?>
								</option>
							</select>
						</div>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<div class="col-sm-12"style="margin-left: 85%;">
							<button type="submit" class="btn btn-info">
								<i class="fas fa-plus"></i> 
								Tambah
							</button>
						</div>  
					</div>
					<!-- /.card-footer -->
				</div>
			</form>
		</div>
	</div>
    <!-- /.card -->
</section>