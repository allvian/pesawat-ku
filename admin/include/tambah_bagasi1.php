<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header shadow-sm p-3 mb-5 bg-white rounded">
			<div class="container">
				<h3 class="card-title"style=" margin-left:415px">
					<i class="far fa-list-alt"></i> Form Tambah Bagasi
				</h3>
			</div>
		</div>
		<div class="card-tools"style="margin-left:78%;">
			<a href="index.php?include=bagasi1" class="btn btn-sm btn-warning ">
				<i class="fas fa-arrow-alt-circle-left"></i> Kembali
			</a>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body container">
		<div class="col-sm-10">
			<?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
				<?php if($_GET['notif']=="tambahkosong"){?>
					<div class="alert alert-danger" role="alert">Maaf data 
						<?php echo $_GET['jenis'];?> wajib di isi
					</div>
				<?php }?>
			<?php }?>
		</div>
		<form class="form-horizontal" method="post" enctype="multipart/form-data" action="index.php?include=konfirmasi_tambah_bagasi1">
			<div class="card-body shadow p-3 mb-5 bg-white rounded">
			   <div class="form-group row">
					<label for="tiket" class="col-sm-3 col-form-label">Kode Bagasi</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" name="bagasi" id="bagasi" value="<?php if(!empty($_SESSION['bagasi'])){ echo $_SESSION['bagasi'];} ?>">
					</div>
				</div>
				<div class="form-group row">
					<label for="pesawat" class="col-sm-3 col-form-label">Kode Pesawat</label>
					<div class="col-sm-7">
						<select class="form-control" id="pesawat" name="pesawat">
							<option value="0">- Pilih Pesawat -</option>
							   <?php
								   $sql_j = "select `kode_pesawat`, `nama_pesawat` from `tb_pesawat` order by `kode_pesawat`";
								   $query_j = mysqli_query($koneksi,$sql_j);
										while($data_j = mysqli_fetch_row($query_j)){
											$kode_pesawat = $data_j[0];
											$nama_pesawat = $data_j[1];
							   ?>
							<option value="<?php echo $kode_pesawat;?>"<?php if(!empty($_SESSION['pesawat'])){if($kode_pesawat==$_SESSION['pesawat']){?> selected="selected" <?php }}?>>
								<?php echo $kode_pesawat;?>
										<?php }?>
							</option>
						</select>
					</div>
				</div>
                <div class="form-group row">
					<label for="berat" class="col-sm-3 col-form-label">Berat Bagasi</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" name="berat" id="berat" value="<?php if(!empty($_SESSION['berat'])){ echo $_SESSION['berat'];} ?>">
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
				<div class="col-sm-10" style="margin-left: 85%;">
					<button type="submit" class="btn btn-info ">
						<i class="fas fa-plus"></i> 
						Tambah
					</button>
				</div>  
			 </div>
			<!-- /.card-footer -->
			 </div>
			</form>
		</div>
	</div>
    <!-- /.card -->
</section>