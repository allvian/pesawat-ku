<?php 
	include('../koneksi/koneksi.php');
	if(isset($_GET['data'])){
		$kode_pesawat = $_GET['data'];
		$_SESSION['kode_pesawat']=$kode_pesawat;
		//get data cabang
		$sql_m = "select `kode_pesawat`,`nama_pesawat` from `tb_pesawat` where `kode_pesawat` = '$kode_pesawat'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$kode_pesawat= $data_m[0];
			$pesawat = $data_m[1];
		} 
	}
?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
			<div class="col-sm-6">
				<h3>
					<i class="fas fa-edit"></i> Edit Pesawat
				</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item">
						<a href="#">Home</a>
					</li>
					<li class="breadcrumb-item">
						<a href="index.php?include=pesawat">Pesawat</a>
					</li>
					<li class="breadcrumb-item active">Edit Pesawat</li>
				</ol>
			</div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"style="margin-top:5px;">
				<i class="far fa-list-alt"></i> Form Edit Pesawat
			</h3>
			<div class="card-tools">
				<a href="index.php?include=pesawat" class="btn btn-sm btn-warning float-right">
					<i class="fas fa-arrow-alt-circle-left"></i> Kembali
				</a>
			</div>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<?php if(!empty($_GET['notif'])){?>
			<?php if($_GET['notif']=="editkosong"){?>
				<div class="alert alert-danger" role="alert">Maaf data Pesawat wajib di isi</div>
			<?php }?>
		<?php }?>
		<form class="form-horizontal" action="index.php?include=konfirmasi_edit_pesawat" method="post">
	        <div class="card-body">
				<div class="form-group row">
					<label for="hobi" class="col-sm-3 col-form-label">Kode Pesawat</label>
						<div class="col-sm-7">
							<input type="text" name="kode_pesawat"  class="form-control" id="kode_pesawat" value="<?php echo $kode_pesawat;?>" readonly="readonly">
						</div>
				</div>
				<div class="form-group row">
					<label for="pesawat"class="col-sm-3 col-form-label">Nama Pesawat</label>
					<div class="col-sm-7">
						<input type="text" class="form-control"id="pesawat" name="pesawat" value="<?php echo $pesawat;?>">
					</div>
				</div>
	        </div>
	        <!-- /.card-body -->
	        <div class="card-footer">
				<div class="col-sm-10">
					<button type="submit" class="btn btn-info float-right">
						<i class="fas fa-save"></i> Simpan
					</button>
				</div>  
	        </div>
			<!-- /.card-footer -->
		</form>
    </div>
    <!-- /.card -->
</section>
