<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header shadow-sm p-3 mb-5 bg-white rounded">
			<div class="container ">
				<h3 class="card-title"style=" margin-left:405px">
					<i class="far fa-list-alt"></i> Form Tambah Pesawat
				</h3>
			</div>	
		</div>
		<div class="card-tools" style="margin-left:78%; margin-bottom:40px">
			<a href="index.php?include=pesawat1" class="btn btn-sm btn-warning ">
				<i class="fas fa-arrow-alt-circle-left"></i> Kembali
			</a>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<?php if(!empty($_GET['notif'])){?>
			<?php if($_GET['notif']=="tambahkosong"){?>
				<div class="alert alert-danger" role="alert">Maaf data pesawat wajib di isi</div>
			<?php }?>
		<?php }?>
		<form class="form-horizontal" method="post" action="index.php?include=konfirmasi_tambah_pesawat1">
			<div class="card-body container shadow p-3 mb-5 bg-white rounded ">
				<div class="form-group row">
					<label for="Pesawat" class="col-sm-3 col-form-label">Kode Pesawat</label>
					<div class="col-sm-7">
						<input type="text" name="kode_pesawat"  class="form-control" id="kode_pesawat" value="<?php if(!empty($_SESSION['kode_pesawat'])){ echo $_SESSION['kode_pesawat'];} ?>">
					</div>
				</div>
				<div class="form-group row">
					<label for="pesawat" class="col-sm-3 col-form-label">Pesawat</label>
					<div class="col-sm-7">
						<input type="text" name="nama_pesawat"  class="form-control" id="nama_pesawat" value="<?php if(!empty($_SESSION['nama_pesawat'])){ echo $_SESSION['nama_pesawat'];} ?>">
					</div>
				</div>
	        </div>
	        <!-- /.card-body -->
	        <div class="card-footer ">
				<div class="col-sm-10" style="margin-left:78%;">
					<button type="submit" class="btn btn-info">
						<i class="fas fa-plus"></i> 
						Tambah
					</button>
				</div>  
	        </div>
	        <!-- /.card-footer -->
	     </form>
    </div>
    <!-- /.card -->
</section>