<?php 
	if(isset($_GET['data'])){
		$kode_tiket = $_GET['data'];
		$_SESSION['kode_tiket']=$kode_tiket;
		//get data mahasiswa
		$sql_m = "select * from `tb_pemesanan` where `kode_tiket` = '$kode_tiket'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$kode_tiket = $data_m[0];
            $nama_pesawat = $data_m[1];
            $tipe_pesawat = $data_m[2];
            $jam_penerbangan = $data_m[3];
		}
		
	}
?>
<section class="content-header">
    <div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3>
					<i class="fas fa-edit"></i> Edit Data Mahasiswa
				</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item">
						<a href="#">Home</a>
					</li>
					<li class="breadcrumb-item">
						<a href="index.php?include=pemesanan">Data pemesanan</a>
					</li>
					<li class="breadcrumb-item active">Edit Data Pemesanan</li>
				</ol>
			</div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="card card-info">
		<div class="card-header">
			<h3 class="card-title"style="margin-top:5px;">
				<i class="far fa-list-alt"></i> Form Edit Data Maasiswa
			</h3>
			<div class="card-tools">
				<a href="index.php?include=pemesanan" class="btn btn-sm btn-warning float-right">
					<i class="fas fa-arrow-alt-circle-left"></i> Kembali
				</a>
			</div>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		</br></br>
		<div class="col-sm-10">
			<?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
				<?php if($_GET['notif']=="editkosong"){?>
					<div class="alert alert-danger" role="alert">Maaf data <?php echo $_GET['jenis'];?> wajib di isi</div>
				<?php }?>
			<?php }?>
		</div>
		<form class="form-horizontal" method="post" enctype="multipart/form-data" action="index.php?include=konfirmasi_edit_pemesanan">
			<div class="card-body">
				<div class="form-group row">
					<label for="foto" class="col-sm-12 col-form-label">
						<span class="text-info">
							<i class="fas fa-user-circle"></i>
							<u>Data pemesanan</u>
						</span>
					</label>
				</div>
				<div class="form-group row">
					<label for="nim" class="col-sm-3 col-form-label">Kode Tiket</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" name="tiket" id="tiket" value="<?php echo $kode_tiket;?>" readonly="readonly">
					</div>
				</div>
				
				<div class="form-group row">
					<label for="jurusan" class="col-sm-3 col-form-label">Pesawat</label>
					<div class="col-sm-7">
						<select class="form-control" id="pesawat" name="pesawat">
							<option value="0">- Pilih pesawat -</option>
							<?php
								$sql_j = "select `kode_pesawat`, `nama_pesawat` from `tb_pesawat`order by `kode_pesawat`";
								$query_j = mysqli_query($koneksi,$sql_j);
								while($data_j = mysqli_fetch_row($query_j)){
									$kode_pesawat = $data_j[0];
									$pesawat = $data_j[1];
							?>
							<option value="<?php echo $kode_pesawat;?>"<?php if($kode_pesawat==$nama_pesawat){?> selected="selected" <?php }?>>
								<?php echo $nama_pesawat;?><?php }?>
							</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="jurusan" class="col-sm-3 col-form-label">Tipe Pesawat</label>
					<div class="col-sm-7">
						<select class="form-control" id="tipe" name="tipe">
							<option value="0">- Pilih Tipe pesawat -</option>
							<?php
								$sql_j = "select * from `tb_tipe_pesawat`order by `kode_tipe_pesawat`";
								$query_j = mysqli_query($koneksi,$sql_j);
								while($data_j = mysqli_fetch_row($query_j)){
									$kode_tipe = $data_j[0];
									$tipe = $data_j[1];
							?>
							<option value="<?php echo $kode_tipe;?>"<?php if($kode_tipe==$tipe_pesawat){?> selected="selected" <?php }?>>
								<?php echo $tipe;?><?php }?>
							</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="jurusan" class="col-sm-3 col-form-label">Jam Penerangan</label>
					<div class="col-sm-7">
						<select class="form-control" id="jam" name="jam">
							<option value="0">- Pilih Jam Penerbangan -</option>
							<?php
								$sql_j = "select * from `tb_penerbangan`order by `kode_penerbangan`";
								$query_j = mysqli_query($koneksi,$sql_j);
								while($data_j = mysqli_fetch_row($query_j)){
									$kode_jam= $data_j[0];
									$jam = $data_j[1];
							?>
							<option value="<?php echo $kode_jam;?>"<?php if($kode_jam==$jam_penerbangan){?> selected="selected" <?php }?>>
								<?php echo $jam;?><?php }?>
							</option>
						</select>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-info float-right">
						<i class="fas fa-plus"></i> Tambah
					</button>
				</div>  
			</div>
			<!-- /.card-footer -->
		</form>
    </div>
    <!-- /.card -->
</section>