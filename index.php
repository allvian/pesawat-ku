<!doctype html>
			<?php 
				session_start();
				include('koneksi/koneksi.php');
				if(isset($_GET["include"])){
					$include = $_GET["include"];
				if($include=="konfirmasi_tambah_pesawat1"){
						include("admin/include/konfirmasi_tambah_pesawat1.php");
					}else if($include=="konfirmasi_tambah_pemesanan1"){
						include("admin/include/konfirmasi_tambah_pemesanan1.php");
					}else if($include=="konfirmasi_tambah_bagasi1"){
						include("admin/include/konfirmasi_tambah_bagasi1.php");
					}else if($include=="konfirmasi_tambah_user1"){
						include("admin/include/konfirmasi_tambah_user1.php");
					}
				}
			?>
<html lang="en">
	<head>
		<?php include("admin/includes/head.php") ?>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<?php include("admin/includes/head.php") ?>
	</head>
		<?php include("admin/includes/nav.php") ?>
			<body>
			<div>
				<div class="container">
				
				<?php
					//jika ada get include
					if(isset($_GET["include"])){
					$include = $_GET["include"];
					//jika ada session id admin
						//pemanggilan ke halaman-halaman menu admin
				?>
				<div class="wrapper">
					<!-- Content Wrapper. Contains page content -->
						<div class="content-wrapper">
						<?php
							if($include=="pesawat1"){
								include("admin/include/pesawat1.php");
							}else if($include=="tambah_pesawat1"){
								include("admin/include/tambah_pesawat1.php");
							}else if($include=="pemesanan1"){
								include("admin/include/pemesanan1.php");
							}else if($include=="tambah_pemesanan1"){
								include("admin/include/tambah_pemesanan1.php");
							}else if($include=="bagasi1"){
								include("admin/include/bagasi1.php");
							}else if($include=="tambah_bagasi1"){
								include("admin/include/tambah_bagasi1.php");
							}else if($include=="home"){
								include("admin/include/home.php");
							}else if($include=="tambah_user1"){
								include("admin/include/tambah_user1.php");
							}
						?>
							<!---/Content--->
						<?php    
							}else{
								//jika tidak ada include pemanggilan halaman form login
							include("admin/include/home.php");
							}
						?>
						
					</div>
					<!---/Content wrapper--->
					<!-- ./wrapper -->
					
				</div>
				<!-- Optional JavaScript -->
				<!-- jQuery first, then Popper.js, then Bootstrap JS -->
				<?php include("admin/includes/script.php") ?>
				<script
				src="https://code.jquery.com/jquery-3.3.1.min.js"
				integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
				crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
				<script src="js/script.js"></script>
				</div>
				<?php include("admin/includes/footer.php") ?>
				</div>
			</body>
</html>