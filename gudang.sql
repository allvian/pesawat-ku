-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Jun 2021 pada 16.33
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gudang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `username`, `password`, `level`) VALUES
(1, 'Alvian Rheiza Ghata Abimanyu', 'rheizaalvian@gmail.com', 'alvian', 'e8cb77839eba5ec65525e642c3899b3b', 'superadmin'),
(2, 'naivla', 'naivla@gmail.com', 'naivla', 'naivla', 'superadmin'),
(24, 'naivla', 'naivla@gmail.com', 'naivla', 'naivla', 'superadmin'),
(222222, 'bastomy', '4bastomygmail.com', 'bastomy', '1d0475709fae2670b458f2991e942708', 'admin'),
(333333, 'miftaudin', 'miftaudin@gmail.com', 'miftaudin', 'd2643e8eea632c95043baddd40fca406', 'superadmin'),
(333334, 'gerio', 'gerio@gmail.com', 'gerio', '5b5651f7c57a6a44693e3ac3920a3f18', 'admin'),
(333335, 'udin', 'udin@gmail.com', 'udin', '6bec9c852847242e384a4d5ac0962ba0', 'superadmin'),
(333336, 'paijo', 'paijo@gmail.com', 'paijo', '44529fdc8afb86d58c6c02cd00c02e43', 'superadmin'),
(333337, 'bejo', 'bejo@gmail.com', 'bejo', 'd4c01b1d3471a1b41ad485918d2298cb', 'admin'),
(333338, 'karjo', 'karjo@gmail.com', 'karjo', '27c634e70d9fa3091558e402a505cb9f', 'admin'),
(333339, 'lila', 'lila @gmail.com', 'lila', 'fda6ef9f6ba8382c875468cd70d33ecf', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_bagasi`
--

CREATE TABLE `tb_bagasi` (
  `kode_pesawat` varchar(10) NOT NULL,
  `kode_bagasi` varchar(10) NOT NULL,
  `berat_bagasi` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_bagasi`
--

INSERT INTO `tb_bagasi` (`kode_pesawat`, `kode_bagasi`, `berat_bagasi`) VALUES
('W003', '111', '5kg'),
('W002', '333', '6kg'),
('W001', 'B001', '10 kg'),
('W003', 'swadwd', '5kg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `kode_tiket` varchar(10) NOT NULL,
  `nama_pesawat` varchar(100) NOT NULL,
  `tipe_pesawat` varchar(100) NOT NULL,
  `jam_penerbangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`kode_tiket`, `nama_pesawat`, `tipe_pesawat`, `jam_penerbangan`) VALUES
('T001', 'W001', 'T001', 'P002'),
('T002', 'W003', 'T004', 'P003'),
('w060', 'W001', 'T002', 'P004'),
('w06999', 'W002', 'T004', 'P004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penerbangan`
--

CREATE TABLE `tb_penerbangan` (
  `kode_penerbangan` varchar(10) NOT NULL,
  `jam_penerbangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_penerbangan`
--

INSERT INTO `tb_penerbangan` (`kode_penerbangan`, `jam_penerbangan`) VALUES
('P001', '05.00 WIB'),
('P002', '07.00 WIB'),
('P003', '09.00 WIB'),
('P004', '11.00 WIB');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesawat`
--

CREATE TABLE `tb_pesawat` (
  `kode_pesawat` varchar(10) NOT NULL,
  `nama_pesawat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pesawat`
--

INSERT INTO `tb_pesawat` (`kode_pesawat`, `nama_pesawat`) VALUES
('0025', 'lion fire'),
('69', 'citylink'),
('W001', 'Lion Air'),
('W002', 'Citilink'),
('W003', 'Garuda Indonesia'),
('W005', 'alskdjlawd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tipe_pesawat`
--

CREATE TABLE `tb_tipe_pesawat` (
  `kode_tipe_pesawat` varchar(10) NOT NULL,
  `tipe_pesawat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_tipe_pesawat`
--

INSERT INTO `tb_tipe_pesawat` (`kode_tipe_pesawat`, `tipe_pesawat`) VALUES
('T001', 'Ekonomi'),
('T002', 'Kelas Bisnis'),
('T003', 'Kelas Super Bisnis'),
('T004', 'Kelas Pertama');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_bagasi`
--
ALTER TABLE `tb_bagasi`
  ADD PRIMARY KEY (`kode_bagasi`),
  ADD KEY `kode_pesawat` (`kode_pesawat`);

--
-- Indeks untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`kode_tiket`),
  ADD KEY `nama_pesawat` (`nama_pesawat`),
  ADD KEY `tipe_pesawat` (`tipe_pesawat`),
  ADD KEY `jam_penerbangan` (`jam_penerbangan`);

--
-- Indeks untuk tabel `tb_penerbangan`
--
ALTER TABLE `tb_penerbangan`
  ADD PRIMARY KEY (`kode_penerbangan`);

--
-- Indeks untuk tabel `tb_pesawat`
--
ALTER TABLE `tb_pesawat`
  ADD PRIMARY KEY (`kode_pesawat`);

--
-- Indeks untuk tabel `tb_tipe_pesawat`
--
ALTER TABLE `tb_tipe_pesawat`
  ADD PRIMARY KEY (`kode_tipe_pesawat`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333340;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_bagasi`
--
ALTER TABLE `tb_bagasi`
  ADD CONSTRAINT `tb_bagasi_ibfk_1` FOREIGN KEY (`kode_pesawat`) REFERENCES `tb_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD CONSTRAINT `tb_pemesanan_ibfk_1` FOREIGN KEY (`nama_pesawat`) REFERENCES `tb_pesawat` (`kode_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_pemesanan_ibfk_2` FOREIGN KEY (`tipe_pesawat`) REFERENCES `tb_tipe_pesawat` (`kode_tipe_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_pemesanan_ibfk_3` FOREIGN KEY (`jam_penerbangan`) REFERENCES `tb_penerbangan` (`kode_penerbangan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
